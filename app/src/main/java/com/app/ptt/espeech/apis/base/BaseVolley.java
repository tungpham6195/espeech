package com.app.ptt.espeech.apis.base;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:20 PM
 * @description
 * @update_date
 */

public class BaseVolley {
    private static BaseVolley mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;

    private BaseVolley(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();


    }

    public static synchronized BaseVolley getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new BaseVolley(context);
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
