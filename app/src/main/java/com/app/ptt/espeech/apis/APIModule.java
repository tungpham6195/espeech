package com.app.ptt.espeech.apis;

import com.app.ptt.espeech.apis.text.TextRepos;
import com.app.ptt.espeech.apis.text.TextReposImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:43 PM
 * @description
 * @update_date
 */
@Module
public class APIModule implements APIComponent {
    @Provides
    @Singleton
    @Override
    public TextRepos provideTextRepos() {
        return new TextReposImpl();
    }


}
