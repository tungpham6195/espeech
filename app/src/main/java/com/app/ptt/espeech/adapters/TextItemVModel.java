package com.app.ptt.espeech.adapters;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.app.ptt.espeech.BR;
import com.app.ptt.espeech.models.Text;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:12 PM
 * @description
 * @update_date
 */

public class TextItemVModel extends BaseObservable {
    Text text;
    int bg_color;
    int text_color;
    Context mContext;

    public TextItemVModel(Context mContext) {
        this.mContext = mContext;
    }

    @Bindable
    public int getText_color() {
        return text_color;
    }

    public TextItemVModel setText_color() {
        this.text_color = text.getSpeak_count() >= 1 ? mContext.getResources().getColor(android.R.color.white) :
                mContext.getResources().getColor(android.R.color.black);
        notifyPropertyChanged(BR.textItemVModel);
        return this;
    }

    @Bindable
    public int getBg_color() {
        return bg_color;
    }

    public TextItemVModel setBg_color() {
        this.bg_color = text.getSpeak_count() >= 1 ?
                mContext.getApplicationContext().getResources().getColor(android.R.color.holo_green_light)
                : mContext.getResources().getColor(android.R.color.white);
        notifyPropertyChanged(BR.textItemVModel);
        return this;
    }

    @Bindable
    public Text getText() {
        return text;
    }

    public TextItemVModel setText(Text text) {
        this.text = text;
        notifyPropertyChanged(BR.textItemVModel);
        return this;
    }
}
