package com.app.ptt.espeech.apis;

import com.app.ptt.espeech.apis.text.TextRepos;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:42 PM
 * @description
 * @update_date
 */
@Component(modules = {APIModule.class})
@Singleton
public interface APIComponent {
    TextRepos provideTextRepos();
}
