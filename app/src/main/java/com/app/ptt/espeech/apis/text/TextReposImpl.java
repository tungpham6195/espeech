package com.app.ptt.espeech.apis.text;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.ptt.espeech.apis.base.BaseVolley;
import com.app.ptt.espeech.models.Text;
import com.app.ptt.espeech.models.TextArray;
import com.google.gson.Gson;

import org.json.JSONObject;

import javax.inject.Inject;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:25 PM
 * @description
 * @update_date
 */

public class TextReposImpl implements TextRepos {
    @Inject
    public TextReposImpl() {
    }

    @Override
    public void getText(Context context, final TextCallBack callBack) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                "https://s3-ap-southeast-1.amazonaws.com/hoan-xtaypro/document/text.json",
                null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response != null) {
                    TextArray textArray = new Gson().fromJson(response.toString(), TextArray.class);
                    for (String item : textArray.getData()) {
                        Text object = new Text().setText(item);
                        callBack.onReponse(object);
                    }
                } else {
                    callBack.onFailure("response != null");
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onError(error.getMessage());

            }
        });
        BaseVolley.getInstance(context.getApplicationContext()).addToRequestQueue(request);
    }
}
