package com.app.ptt.espeech.models;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:14 PM
 * @description
 * @update_date
 */

public class Text {
    private String text;
    private int speak_count = 0;
    private boolean isSpoken = false;

    public boolean isSpoken() {
        return isSpoken;
    }

    public Text setSpoken(boolean spoken) {
        isSpoken = spoken;
        return this;
    }

    public String getText() {
        return text;
    }

    public Text setText(String text) {
        this.text = text;
        return this;
    }

    public int getSpeak_count() {
        return speak_count;
    }

    public Text setSpeak_count(int speak_count) {
        this.speak_count = speak_count;
        return this;
    }
}
