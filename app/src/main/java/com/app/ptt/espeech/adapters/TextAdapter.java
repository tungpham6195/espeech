package com.app.ptt.espeech.adapters;

import android.animation.Animator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;

import com.app.ptt.espeech.R;
import com.app.ptt.espeech.databinding.LayoutTextItemBinding;
import com.app.ptt.espeech.models.Text;

import java.util.ArrayList;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:10 PM
 * @description
 * @update_date
 */

public class TextAdapter extends RecyclerView.Adapter<TextAdapter.ViewHolder> {
    private ArrayList<Text> texts;
    private Context mContext;

    public TextAdapter(Context mContext) {
        this.mContext = mContext;
        this.texts = new ArrayList<>();
    }

    public ArrayList<Text> getTexts() {
        return texts;
    }

    public TextAdapter addText(Text text) {
        this.texts.add(text);
        notifyItemInserted(this.texts.size() - 1);
        return this;
    }

    public TextAdapter updateText(int position, int count) {
        int curr_count = this.texts.get(position).getSpeak_count();
        this.texts.get(position).setSpeak_count(curr_count + count);
        notifyItemChanged(position);
        return this;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_text_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final TextItemVModel model = new TextItemVModel(mContext).setText(texts.get(position))
                .setBg_color()
                .setText_color();
        holder.getBinding().setTextItemVModel(model);
        holder.getBinding().executePendingBindings();
        holder.getBinding().cardvPhraseItem.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    model.setText_color()
                            .setBg_color();
                    int cx = (holder.binding.getRoot().getLeft() + holder.binding.getRoot().getRight()) / 2;
                    int cy = (holder.binding.getRoot().getTop() + holder.binding.getRoot().getBottom()) / 2;
                    int initialRadius = Math.max(holder.binding.getRoot().getWidth(),
                            holder.binding.getRoot().getHeight());
                    Animator anim = ViewAnimationUtils.createCircularReveal(holder.getBinding().getRoot(),
                            cx, cy, 0, initialRadius);
                    anim.setDuration(300);
                    anim.setStartDelay(100 + 300 * holder.getAdapterPosition());
                    anim.start();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return texts.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private LayoutTextItemBinding binding = null;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView.getRootView());
        }

        public LayoutTextItemBinding getBinding() {
            return binding;
        }
    }
}
