package com.app.ptt.espeech.activities.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.app.ptt.espeech.R;
import com.app.ptt.espeech.activities.speech.SpeechActivity;
import com.app.ptt.espeech.adapters.TextAdapter;
import com.app.ptt.espeech.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private MainActivityVModel mainActivityVModel;
    private ActivityMainBinding binding;
    public static final int REQ_CODE_SPEECH_ACT = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivityVModel = new MainActivityVModel()
                .setTextAdapter(new TextAdapter(this.getApplicationContext()))
                .setmContext(this.getApplicationContext())
                .setOnClickListener(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setMainActivityVModel(mainActivityVModel);
        binding.executePendingBindings();
        mainActivityVModel.requestGetTextList();

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_speech:
                ActivityOptionsCompat compat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        this, findViewById(R.id.btn_speech), "speech_toolbar");
                startActivityForResult(new Intent(this, SpeechActivity.class),
                        REQ_CODE_SPEECH_ACT, compat.toBundle());
                return;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_ACT:
                if (resultCode == RESULT_OK && data != null) {
                    mainActivityVModel.countPhrase(data.getStringExtra("result"));
                }
                return;
        }
    }
}
