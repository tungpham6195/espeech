package com.app.ptt.espeech.models;

import java.util.ArrayList;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:14 PM
 * @description
 * @update_date
 */

public class TextArray {
    private ArrayList<String> data;

    public ArrayList<String> getData() {
        return data;
    }

    public TextArray setData(ArrayList<String> data) {
        this.data = data;
        return this;
    }
}
