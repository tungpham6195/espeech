package com.app.ptt.espeech.apis.text;

import com.app.ptt.espeech.models.Text;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:23 PM
 * @description
 * @update_date
 */

public interface TextCallBack {
    void onReponse(Text object);

    void onFailure(String message);

    void onError(String error);
}
