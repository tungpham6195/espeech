package com.app.ptt.espeech.activities.main;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.app.ptt.espeech.BR;
import com.app.ptt.espeech.adapters.TextAdapter;
import com.app.ptt.espeech.apis.DaggerAPIComponent;
import com.app.ptt.espeech.apis.text.TextCallBack;
import com.app.ptt.espeech.apis.text.TextRepos;
import com.app.ptt.espeech.models.Text;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:05 PM
 * @description
 * @update_date
 */

public class MainActivityVModel extends BaseObservable implements TextCallBack {
    private Context mContext;
    private TextAdapter textAdapter;
    private View.OnClickListener onClickListener;

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public MainActivityVModel setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        return this;
    }

    @Inject
    TextRepos textRepos;

    public MainActivityVModel() {
        textRepos = DaggerAPIComponent.create().provideTextRepos();
    }

    public MainActivityVModel setmContext(Context mContext) {
        this.mContext = mContext;
        return this;
    }

    @Bindable
    public TextAdapter getTextAdapter() {
        return textAdapter;
    }

    public MainActivityVModel setTextAdapter(TextAdapter textAdapter) {
        this.textAdapter = textAdapter;
        notifyPropertyChanged(BR.mainActivityVModel);
        return this;
    }

    @BindingAdapter("main:textAdapter")
    public static void setTextAdater(RecyclerView rv, TextAdapter textAdapter) {
        rv.setLayoutManager(new GridLayoutManager(rv.getContext(), 2,
                LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(textAdapter);
    }

    public void countPhrase(String text) {
            for (Text item : textAdapter.getTexts()) {
                if (item.getText().toUpperCase().equals(text.toUpperCase())) {
                    textAdapter.updateText(textAdapter.getTexts().indexOf(item), 1);
                    break;
                }
            }
    }

    public void requestGetTextList() {
        textRepos.getText(mContext, this);
    }

    @Override
    public void onReponse(Text object) {
        textAdapter.addText(object);
    }

    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onError(String error) {

    }
}
