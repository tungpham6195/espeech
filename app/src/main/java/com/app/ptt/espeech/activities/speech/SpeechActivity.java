package com.app.ptt.espeech.activities.speech;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.app.ptt.espeech.R;
import com.app.ptt.espeech.databinding.ActivitySpeechBinding;

import java.util.Locale;

public class SpeechActivity extends AppCompatActivity {
    private static final int REQ_CODE_SPEECH_INPUT = 102;
    private SpeechActivityVModel speechActivityVModel;
    private ActivitySpeechBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        speechActivityVModel = new SpeechActivityVModel();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_speech);
        binding.setSpeechActivityVModel(speechActivityVModel);
        binding.executePendingBindings();
        setSupportActionBar(binding.toolbar);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                openMic();
            }
        }, 6 * 100);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(RESULT_CANCELED);
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openMic() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak here!");
        startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT:
                if (resultCode == RESULT_OK && data != null) {
                    speechActivityVModel.setText(data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
                    binding.userSpeech.setText("\"" + data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0) + "\"");
                    Log.d(this.getClass().getSimpleName(), data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0));
                    setResult(RESULT_OK, new Intent().putExtra("result",
                            data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0)));
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onBackPressed();
                        }
                    }, 1 * 1000);
                }
                return;
        }
    }
}
