package com.app.ptt.espeech.apis.text;

import android.content.Context;

/**
 * @author PTT
 * @date 2018-04-02
 * @time 9:22 PM
 * @description
 * @update_date
 */

public interface TextRepos {
    void getText(Context context, TextCallBack callBack);
}
