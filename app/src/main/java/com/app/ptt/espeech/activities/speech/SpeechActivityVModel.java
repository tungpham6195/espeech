package com.app.ptt.espeech.activities.speech;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.app.ptt.espeech.BR;

/**
 * @author PTT
 * @date 2018-04-03
 * @time 3:17 PM
 * @description
 * @modify-date
 */

public class SpeechActivityVModel extends BaseObservable {
    private String text = "";

    @Bindable
    public String getText() {
        return text;
    }

    public SpeechActivityVModel setText(String text) {
        this.text = text;
        notifyPropertyChanged(BR.speechActivityVModel);
        return this;
    }
}
